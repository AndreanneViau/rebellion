/** Gestion d'une ressource
*/

class Ressource extends Observer{


  PVector ressourcePos;
  String ressourceType;
  float ressourceNb;

  boolean alive;

  int id;
  
  Ressource(PVector NressourcePos, String NressourceType, float NressourceNb) {
    
    obs = new ArrayList<Observable>();
    
    ressourcePos = new PVector();
    ressourcePos.x = NressourcePos.x;
    ressourcePos.y = NressourcePos.y;
    ressourceType = NressourceType;
    ressourceNb = NressourceNb;
    
    alive = true;
    
    if(ressourceType == "F"){
      politician.alterAnger(RESSOURCE_FOOD_PRICE);
    }else if(ressourceType == "C"){
      politician.alterAnger(RESSOURCE_COAL_PRICE);
    }else if(ressourceType == "R"){
      politician.alterAnger(RESSOURCE_CLOT_PRICE);
    }
    
  }
  
  
  PVector getPos(){return ressourcePos;}
  
  
  void update(){
    
    for(Observable part : obs){
      
      if(part.isNotified()){
        if(((FoodObservable)part).foodID == id){
          alive = false;
          part.confirmNotification();
        }
      }
      
    }
    
  }
  
  void display(){
  
    pushMatrix();
    translate(0, 0, 3);
    
    //DRAW RESSOURCE
    colorMode(RGB, 255, 255, 255);
    stroke(#080908);
    if(ressourceType == "F"){
      fill(#42A0C1);
    }else if(ressourceType == "C"){
      fill(#8542C1);
    }else if(ressourceType == "R"){
      fill(#46FF47);
    }else {
      fill(255, 0, 0);
    }
    ellipse(ressourcePos.x, ressourcePos.y, RESSOURCE_SIZE + ressourceNb/5, RESSOURCE_SIZE + ressourceNb/5);
    
    popMatrix();
  
  }
  
  void listing(){
     println("Ressource posX:", ressourcePos.x, " posY:", ressourcePos.y," Type:",ressourceType," Nb:", ressourceNb);
  }
  
  
  boolean isAlive(){return alive;}
  
 /** Vérifie si un contact a eu lieu avec la ressource
 */
  boolean hasContact(PVector opos, PVector osize){
    
    if(ressourcePos.x >= opos.x && ressourcePos.x <= opos.x + osize.x){
      if(ressourcePos.y >= opos.y && ressourcePos.y <= opos.y + osize.y){
        
        return true;
        
      }
    }
    
    return false;
    
  }
  
  
}

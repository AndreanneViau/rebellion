///
/// Classe pour chacun des noeuds de GOAP (précondition/action/postcondition)
///  


class StepNode{

  int ID;
  String actionName;
  String preCondition;
  String postCondition;
  int cost;
  int gain;
  boolean distFactor;
  boolean needsFactor;
  
  
  
  
  StepNode( int NID, String NactionName, String NpreCondition, String NpostCondition, int Ncost, int Ngain, boolean nDistF, boolean nNeedsF) {
    
    actionName = NactionName;
    preCondition = NpreCondition;
    postCondition = NpostCondition;
    cost = Ncost;
    gain = Ngain;
    distFactor = nDistF;
    needsFactor = nNeedsF;
    ID = NID;
    
  }


  int getCost(int needsIn, float distIn){
    
    if(needsFactor){
      return cost * (100 - needsIn);
    }else if(distFactor){
      return cost + round(distIn);
    }else {
      return cost;
    }
    
  }
  
  int getGain(){
    
    return gain;
    
  }
  
  String getPreCondition(){return preCondition;}
  String getPostCondition(){return postCondition;}
  
  void display(){
  println("ID:", ID, " Name:", actionName," Pre:",preCondition," Post:", postCondition, " Cost:", cost, " Gain:", gain);
  }

}

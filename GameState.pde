/** Observe l'état de satisfaction face au politicien
*/

class GameState extends Observer{
  
  
  boolean gameOver;
  
  
  GameState(){
    
    obs = new ArrayList<Observable>();
    
  }
  
  boolean isOver(){return gameOver;}
  
  
  
  void update(){
    
    for(Observable part : obs){
      
      if(part.isNotified()){
        gameOver = true;
        part.confirmNotification();
      }
      
    }
    
  }
  
  void show(){
    
    pushMatrix();
    translate(0, 0, 16);
    
    textAlign(CENTER, CENTER);
    fill(0);
    textSize(25);
    text("GAME OVER", width/2-2, height/2);
    text("GAME OVER", width/2+2, height/2);
    text("GAME OVER", width/2, height/2-2);
    text("GAME OVER", width/2, height/2+2);
    translate(0, 0, 1);
    fill(255);
    text("GAME OVER", width/2, height/2);
    textAlign(LEFT, BASELINE);
    
    popMatrix();
    
  }
  
  
}

///
/// Gestion du temps
///  

float currentTime = 0;
float previousTime = 0;
float deltaTime = 0;

void calcTime(){
  
  currentTime = millis();
  deltaTime = currentTime - previousTime;
  previousTime = currentTime;
  
}

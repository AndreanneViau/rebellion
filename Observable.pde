/** Patron pour une entité observable qui est observé par un observateur
*/

abstract class Observable{
  

  private boolean notification;
  
/**Avise l'observateur
*/  
  protected void notifyObs(){notification = true;}
 
 /**Avise qu'une notification est en attente
 */
  public boolean isNotified(){return notification;}
  
  /**Réinitialise la notification après qu'elle est été prise en compte
  */
  public void confirmNotification(){notification = false;}
  
}

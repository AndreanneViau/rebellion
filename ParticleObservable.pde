/** Observable qui notifie qu'une particule doit apparaitre
*/


abstract class ParticleObservable extends FoodObservable{
  
  protected int particleID;
  protected PVector spawnPos;
  
  private boolean pnotification;
  
  protected void pnotifyObs(){pnotification = true;}
  
  public boolean pisNotified(){return pnotification;}
  public void pconfirmNotification(){pnotification = false;}
  
}

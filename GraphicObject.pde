///
/// Template pour les éléments graphiques 
/// 


abstract class GraphicObject{

  PVector pos;
  PVector vel;
  PVector acc;
  
  PVector size;
  
  color fillColor;
  color strokeColor;
  color strokeWeight;
  
  
  
  abstract void update(float deltaTime);
  
  abstract void show();
  
  

}

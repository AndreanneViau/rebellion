/** Interface de jeu
*/


class Politician extends Observable{
  
  
  float angerLevel;
  Vignette vignette;
  
  String boldType;
  
  Politician(){
    
    angerLevel = 0;
    vignette = new Vignette();
    
  }
  
  void setBoldType(String type){boldType = type;}
  
  void update(){
    
    if(angerLevel >= 100){
      notifyObs();
      angerLevel = 10000;
    }
    
    if(angerLevel != vignette.getLastDist()){
      vignette.setRatio(floor(angerLevel));
    }
    
  }
  
  
  String press(){
    
    String result = "";
    
    if(mouseX < (height/BUTTONS_WIDTH_RATIO)*3){
      if(mouseY > height - (height/BUTTONS_HEIGHT_RATIO)){
        
        if(mouseX > (height/BUTTONS_WIDTH_RATIO)*2){
          result = "R";
        }else if(mouseX > (height/BUTTONS_WIDTH_RATIO)){
          result = "C";
        }else {
          result = "F";
        }
        
      }
    }
    
    if(result.length() == 1){
      boldType = result;
    }
    
    return result;
    
  }
  
  
  void show(boolean isOver){
    
    vignette.show();
    
    
    if(!isOver){
      
      pushMatrix();
      translate(15, height - height/BUTTONS_HEIGHT_RATIO - 11 - (height/BUTTONS_HEIGHT_RATIO)/2, 17);
      
      stroke(0);
      fill(255);
      strokeWeight(2);
      
      rect(0, 0, (height/BUTTONS_WIDTH_RATIO)*3, (height/BUTTONS_HEIGHT_RATIO)/2);
      
      translate(0, 0, 1);
      
      
      textAlign(CENTER, CENTER);
      fill(#080908);
      textSize(12);
      text("Citizen satisfaction: " + nf(100 - angerLevel, 0, 0) + "%", (height/BUTTONS_WIDTH_RATIO)*1.5, (height/BUTTONS_HEIGHT_RATIO)/4);
      
      translate(0, (height/BUTTONS_HEIGHT_RATIO)/2, -1);
      
      fill(255);
      
      rect(0, 0, (height/BUTTONS_WIDTH_RATIO), height/BUTTONS_HEIGHT_RATIO);
      rect(0, 0, (height/BUTTONS_WIDTH_RATIO) + (height/BUTTONS_WIDTH_RATIO), height/BUTTONS_HEIGHT_RATIO);
      rect(0, 0, (height/BUTTONS_WIDTH_RATIO) + (height/BUTTONS_WIDTH_RATIO)*2, height/BUTTONS_HEIGHT_RATIO);
      
      strokeWeight(1);
      
      colorMode(RGB, 255, 255, 255);
      textAlign(CENTER, TOP);
      stroke(#080908);
      
      translate((height/BUTTONS_WIDTH_RATIO)/2, (height/BUTTONS_HEIGHT_RATIO)/2, 1);
      fill(#42A0C1);
      if(boldType == "F"){
        strokeWeight(3);
        textSize(12);
      }else {
        strokeWeight(1);
        textSize(8);
      }
      
      ellipse(0, 0, RESSOURCE_SIZE*2, RESSOURCE_SIZE*2);
      fill(#080908);
      text("FOOD", 0, RESSOURCE_SIZE);
      
      
      translate(height/BUTTONS_WIDTH_RATIO, 0);
      fill(#8542C1);
      if(boldType == "C"){
        strokeWeight(3);
        textSize(12);
      }else {
        strokeWeight(1);
        textSize(8);
      }
      
      ellipse(0, 0, RESSOURCE_SIZE*2, RESSOURCE_SIZE*2);
      fill(#080908);
      text("COAL", 0, RESSOURCE_SIZE);
      
      translate(height/BUTTONS_WIDTH_RATIO, 0);
      fill(#46FF47);
      if(boldType == "R"){
        strokeWeight(3);
        textSize(12);
      }else {
        strokeWeight(1);
        textSize(8);
      }
      
      ellipse(0, 0, RESSOURCE_SIZE*2, RESSOURCE_SIZE*2);
      fill(#080908);
      text("CLOTHES", 0, RESSOURCE_SIZE);
      
      
      strokeWeight(1);
      
      textAlign(LEFT, BASELINE);
      
      popMatrix();
      
    }
    
  }
  
  /** Gestion de degré de satisfaction des citoyens
  */
  void alterAnger(float amount){
    
    angerLevel += amount/10;
    
    if(angerLevel < 0){
      angerLevel = 0;
    }else if(angerLevel > 100){
      angerLevel = 100;
    }
    
  }
  
  
  
  
}

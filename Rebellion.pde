
void setup () {
  
  mousePosition = new PVector();
  fullScreen(P3D);
 // size (1000, 700, P3D);
  //noSmooth();  //NOT IN P3D 
  
  hint(ENABLE_DEPTH_SORT);
  
  createAll();
  
}

void draw () {
 
  calcTime();
  
  update(deltaTime);
  display();
  
}


void update(float delta) {

  if(!gameState.isOver()){
    
    for (Citizen c : citizens) { 
      c.update(delta);
      c.findRessourcesInRange(ressources.getRessources());
    }
    
    
    ressources.update();
    
    
    citizenFactory.update();
    if(citizenFactory.canAddCitizen()){
      addCitizen();
    }
    
  }
  
  particles.update(delta);
    
  politician.update();
  gameState.update();
  
}


void display() {

  if (currentTime - lastDisplay > FRAME_DELTA) {
    
    background(175);

    if (citizens != null) {
      for (Citizen c : citizens) { 
        c.display();
      }
    }

    if (ressources != null) {
      ressources.show();
    }
    
    particles.show();
    
    politician.show(gameState.isOver());
    
    
    if(gameState.isOver()){
      gameState.show();
    }

    lastDisplay = currentTime;
    
  }
  
}

/**Gestion de la souris pour l'ajout de ressources
*/

void mousePressed() {

  if (mouseButton == LEFT) {
    
    String result = politician.press();
    
    if(result.length() == 1){
      spawnType = result;
    }else {
      ressources.addRessource(ressourceFactory.getRessource(spawnType));
    }
    
  } else if (mouseButton == RIGHT) {
    for (Citizen c : citizens) { 
      c.forceGoHome();
    }
  }
}

/** Gestion des entrées au clavier par l'utilisateur
*/

void keyPressed(){
  
  if(key == ' '){
    
    createAll();
    
  }else if(key == ENTER){
    
    for(int i = 0; i < 10; i++){
      ressources.addRessource(ressourceFactory.getRessource(spawnType + "R"));
    }
    
  }else if(key == 'N' || key == 'n'){
    
    for(Citizen part : citizens){
      part.kill();
    }
    
  }else {
    
    spawnType = str(key).toUpperCase();
    if(spawnType.equalsIgnoreCase("F")){
      spawnType = "F";
    }else if(spawnType.equalsIgnoreCase("C")){
      spawnType = "C";
    }else if(spawnType.equalsIgnoreCase("R")){
      spawnType = "R";
    }
    
    politician.setBoldType(spawnType);
    
  }
  
  
}

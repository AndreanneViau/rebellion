var class_rebellion_1_1_vignette =
[
    [ "Vignette", "class_rebellion_1_1_vignette.html#aede2a690a96aaa5e8b84ba56a158d2e9", null ],
    [ "drawVignette", "class_rebellion_1_1_vignette.html#a330f811b10e3632a88255223acaae8f8", null ],
    [ "flash", "class_rebellion_1_1_vignette.html#a91d2229d1d30d1bb1925c58e891dc97b", null ],
    [ "getLastDist", "class_rebellion_1_1_vignette.html#a561420ceba2bb7521562c01d89ea170b", null ],
    [ "setRatio", "class_rebellion_1_1_vignette.html#aacddbfa7d7bdfd32656e74fd10dedfff", null ],
    [ "show", "class_rebellion_1_1_vignette.html#ab7ee0cf76bcdf5915c696c9f4f7d6e74", null ],
    [ "flashStrength", "class_rebellion_1_1_vignette.html#a863a96697a6b37bc23ce0138cacbfccd", null ],
    [ "lastDist", "class_rebellion_1_1_vignette.html#a65c3d7799955bc8ab872d73fca6c3c10", null ],
    [ "ratio", "class_rebellion_1_1_vignette.html#a4c2e02cae20774464fb7f0d8f79e9579", null ],
    [ "ratioGoal", "class_rebellion_1_1_vignette.html#abe52ba0eaabcd1edf2a1bcb3890e3a6f", null ],
    [ "ratioSpeed", "class_rebellion_1_1_vignette.html#a5ca8ce1d170eadd9e3e841653971ce92", null ],
    [ "vignette", "class_rebellion_1_1_vignette.html#aee8c9f399686495b7978b2cea12a902a", null ]
];
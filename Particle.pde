///
/// Classe template de particules, permet la génération de particules avec des attributs communs
/// 

abstract class Particle extends GraphicObject{
  
  Particle(){
    life = 255;
    
    alive = true;
  }
  
  protected int type;
  protected float life;
  
  protected boolean alive;
  
  
  public boolean isAlive(){return alive;}
  
  
}

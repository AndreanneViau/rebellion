///
/// Utilisation du DP de la fabrique pour générer les ressources afin de permettre de la flexibilité lors de la création, 
/// permet de générer la ressource souhaitée sans répéter le  code pour chaque type de ressource
///


public class RessourceFactory{
  
  public Ressource getRessource(String type){
    
    switch(type){
      case("F"):
        return new Ressource(new PVector(mouseX, mouseY), "F", int(random(20, 40)));
      case("C"):
        return new Ressource(new PVector(mouseX, mouseY), "C", int(random(20, 40)));
      case("R"):
        return new Ressource(new PVector(mouseX, mouseY), "R", int(random(20, 40)));
      case("FR"):
        return new Ressource(new PVector(random(width), random(height)), "F", int(random(20, 40)));
      case("CR"):
        return new Ressource(new PVector(random(width), random(height)), "C", int(random(20, 40)));
      case("RR"):
        return new Ressource(new PVector(random(width), random(height)), "R", int(random(20, 40)));
      default:
        return null;
    }
    
  }
  
}

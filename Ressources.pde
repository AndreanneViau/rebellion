/** Gestionnaire de l'ensemble des ressources sur le plan de jeu
*/

class Ressources{
  
  ArrayList<Ressource> ressources;
  
  Ressources(){
    
    ressources = new ArrayList<Ressource>();
    
  }
  
  ArrayList<Ressource> getRessources(){return ressources;}
  
  
  void update(){
    
    for(int i = ressources.size() - 1; i >= 0; i--){
      
      Ressource part = ressources.get(i);
      
      part.update();
      
      if(!part.isAlive()){
        ressources.remove(i);
      }
      
    }
    
  }
  
  
  void show(){
    
    for (Ressource part : ressources) { 
      part.display();
    }
    
    
  }
  /** Donne tous les observables aux ressources
  */
  void getAllObservables(Ressource ressource){
    
    for(Citizen part : citizens){
      
      ressource.addObservable(part);
      
    }
    
  }
  
  
  void addObservable(Citizen citizen){
    
    for(Ressource part : ressources){
      
      part.addObservable(citizen);
      
    }
    
  }
  
  
  void addRessource(Ressource nressource){
    if(nressource != null){
      getAllObservables(nressource);
      nressource.id = foodAmount++;
      ressources.add(nressource);
    }
  }
  
  void addFood(PVector spawnPos){
    
    Ressource ressource = new Ressource (spawnPos, "F", int(random(20, 40)));
    addRessource(ressource);
    
  }
  
  void addRessourceOnMouse() {
  
    addFood(new PVector(mouseX, mouseY));
    
  }
  
}

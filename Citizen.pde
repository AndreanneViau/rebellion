///
/// Classe gérant le citoyen 
///

class Citizen extends ParticleObservable{
  
  int hungryLevel;
  int coldLevel;
  int ragLevel;
  PVector housePosition;
  PVector citizenPosition;
  PVector pointOfInterest;
  
  ArrayList<Ressource> closeRessources;
  
  //=====ENDURANCE RATES=====\\
  
  float foodAddRate;
  float lastAte;
  
  float coalAddRate;
  float lastCoal;
  
  float ragAddRate;
  float lastRag;
  
  
  float lastGoaped;
  float goapRate;
  
  
  String state;
  String neededRessource;
  
  boolean alive;
  
  
  StateHandler currentState;
  
///
/// Contstructeur du citoyen, on lui attribue sa maison comme point d'intérêt initial
///

  Citizen() {

    housePosition = new PVector();
    citizenPosition = new PVector();
    pointOfInterest = new PVector(housePosition.x, housePosition.y);
    
    closeRessources = new ArrayList<Ressource>();
    
    lastAte = 0;
    generateGaussianValues();
    

    hungryLevel = 0;
    coldLevel =   0;
    ragLevel =    0;

    lastGoaped = 0;
    goapRate = 4000;
    

    alive = true;
    
    currentState = new StateHandler();
    
  }
  
///
/// On tue le citoyen si l'utilisateur n'a pas été en mesure de satisfaire à ses besoins de base
///
  void kill(){
    alive = false;
    makeDeadParticle();
    politician.alterAnger(25);
    
    hungryLevel = 0;
    coldLevel = 0;
    ragLevel = 0;
    
  }
    
///
/// Mise à jour des attributs du citoyen dans le temps en fonction des actions de l'utilisateur et de sa quête de ressources
///
  void update(float delta) {
    
    if(alive){
      
      //=====TIME BASED CHANGES=====\\
      
      //FOOD
      if(currentTime - lastAte >= foodAddRate){
        
        if(hungryLevel < 100){
          hungryLevel++;
        }
        
        lastAte = currentTime;
      }
      
      if(currentTime - lastCoal >= coalAddRate){
        
        if(coldLevel < 100){
          coldLevel++;
        }
        
        lastCoal = currentTime;
      }
      
      if(currentTime - lastRag >= ragAddRate){
        
        if(ragLevel < 100){
          ragLevel++;
        }
        
        lastRag = currentTime;
      }
      
      if(hungryLevel == 100 && coldLevel == 100 && ragLevel == 100){
        kill();
      }
      
      contactOnRessource();
      
      
      // CALCUL GOAP
      
      if(currentTime - lastGoaped >= goapRate){
        findGoal();
        
        lastGoaped = currentTime;
        
      }
  
  
      //=====WALK TO POINT OF INTEREST=====\\
      
      if(currentState.getCurrentState().equalsIgnoreCase("StateFetching") && PVector.dist(pointOfInterest, citizenPosition) < CITIZEN_SPEED){
        makeConfusedParticle();
        politician.alterAnger(5);
        findRessourcesInRange(ressources.getRessources());
        findGoal();
      }else if(currentState.getCurrentState().equalsIgnoreCase("StateToHome") && PVector.dist(pointOfInterest, citizenPosition) < CITIZEN_SPEED){
        citizenPosition = pointOfInterest.copy();
        currentState.atHome();
      }else {
        PVector orientation = PVector.sub(pointOfInterest, citizenPosition).normalize();   
        orientation.setMag(CITIZEN_SPEED);
        citizenPosition.add(orientation);
      }
      
      
    }
    
    
  }
  
///
/// Gestion de l'affichage du citoyen
///

  void display() {

    
    pushMatrix();
    translate(0, 0, 1);
    
    //DRAW HOUSE
    colorMode(RGB, 255, 255, 255);
    fill(#D1D1D1);
    rectMode(CENTER);
    rect(housePosition.x+CITIZEN_RECTANGLE_SIZE, housePosition.y, CITIZEN_HOUSE_SIZE, CITIZEN_HOUSE_SIZE);
    
    popMatrix();

    if(DEBUG){
      stroke(100);
      strokeWeight(1);
      line(citizenPosition.x, citizenPosition.y, pointOfInterest.x, pointOfInterest.y);
    }
    
    
    pushMatrix();
    translate(0, 0, 2);

    stroke (0);
    strokeWeight(1);
    fill (255);

    colorMode(HSB, 360, 255, 255);
    // x    val
    //--- = ---
    //125   100

    rectMode(CENTER);
    textSize(10); 
    colorMode(HSB, 360, 100, 255);
    fill(195, 65, hungryLevel * 255 / 100);
    rect(citizenPosition.x, citizenPosition.y - 8, CITIZEN_RECTANGLE_SIZE, CITIZEN_RECTANGLE_SIZE);
    
    if(DEBUG && alive){
      colorMode(RGB, 255, 255, 255);
      fill (0);
      text(nf(hungryLevel, 2, 0), citizenPosition.x + CITIZEN_RECTANGLE_SIZE, citizenPosition.y-8 +(CITIZEN_RECTANGLE_SIZE/2));
    }
    
    colorMode(HSB, 360, 100, 255);
    fill(271, 65, coldLevel * 255 / 100);
    rect(citizenPosition.x, citizenPosition.y, CITIZEN_RECTANGLE_SIZE, CITIZEN_RECTANGLE_SIZE);
    
    if(DEBUG && alive){
      colorMode(RGB, 255, 255, 255);
      fill (0);
      text(nf(coldLevel, 2, 0), citizenPosition.x + CITIZEN_RECTANGLE_SIZE, citizenPosition.y+(CITIZEN_RECTANGLE_SIZE/2));
    }
    
    colorMode(HSB, 360, 100, 255);
    fill(120, 72, ragLevel * 255 / 100);
    rect(citizenPosition.x, citizenPosition.y + 8, CITIZEN_RECTANGLE_SIZE, CITIZEN_RECTANGLE_SIZE);
    
    if(DEBUG && alive){
      colorMode(RGB, 255, 255, 255);
      fill (0);
      text(nf(ragLevel, 2, 0), citizenPosition.x + CITIZEN_RECTANGLE_SIZE, citizenPosition.y+8+(CITIZEN_RECTANGLE_SIZE/2));
    }
    
    popMatrix();
    
    
    rectMode(CORNER);
    

    colorMode(RGB, 255, 255, 255);
  }

///
/// Assignation de la maison comme point d'intérêt 
///
  void forceGoHome() {
    pointOfInterest = housePosition.copy();
    currentState.goHome();
  }
  
  void findRessourcesInRange(ArrayList<Ressource> ressources){
    
    closeRessources.clear();
    
    float dist;
    float closeDist = width/CITIZEN_RESSOURCE_RANGE_RATIO;
    
    for(int i = 0; i < ressources.size(); i++){
      
      Ressource part = ressources.get(i);
      
      dist = PVector.dist(citizenPosition, part.getPos());
      if(dist < closeDist){
        closeRessources.add(part);
      }
      
    }
    
  }
  
///
/// Détection des ressources à proximité du citoyen afin de déterminer sa région d'action
///  
  void findClosestRessource(){
    
    int closestIndex = -1;
    
    float dist;
    float closestDist = Float.POSITIVE_INFINITY;
    
    for(int i = 0; i < closeRessources.size(); i++){
      
      Ressource part = closeRessources.get(i);
      
      dist = PVector.dist(citizenPosition, part.getPos());
      if(dist < closestDist && part.ressourceType == neededRessource){
        closestDist = dist;
        closestIndex = i;
      }
      
    }
    
    
    if(closestIndex != -1){
      
      Ressource closest = closeRessources.get(closestIndex);
      pointOfInterest = closest.getPos().copy();
      currentState.fetching();
      
    }else {
      forceGoHome();
    }
    
  }
  
/** Informe le GOAP si le citoyen a une ressource à proximité
*/
  
  boolean hasRessourceInRange(String ressourceName){
    
    boolean foundOne = false;
    
    for(Ressource part : closeRessources){
      
      if(part.ressourceType == ressourceName){
        foundOne = true;
      }
      
    }
    
    return foundOne;
    
  }
  
  /** Calcul de la distance de la ressource la plus proche
  */
  
  float distToClosest(String ressourceName){
    
    float closeset = 1000000;
    
    for(Ressource part : closeRessources){
      
      if(part.ressourceType == ressourceName){
        float dist = PVector.dist(part.getPos(), citizenPosition);
        if(dist < closeset){
          closeset = dist;
        }
      }
      
    }
    
    
    return closeset;
    
  }
  
///
/// Le citoyen récupère une ressource sur le plan de jeu
///  
  void contactOnRessource(){
    
    PVector size = new PVector(CITIZEN_RECTANGLE_SIZE, CITIZEN_RECTANGLE_SIZE*3);
    PVector pos = new PVector(citizenPosition.x - size.x/2, citizenPosition.y - size.y*0.5);
    
    for(Ressource part : closeRessources){
      
      if(part.hasContact(pos, size)){
        notifyObs();
        foodID = part.id;
        
        if(part.ressourceType == neededRessource){
          if(neededRessource == "F"){
            hungryLevel -= part.ressourceNb;
            if(hungryLevel < 0){
              hungryLevel = 0;
            }
          }else if(neededRessource == "C"){
            coldLevel -= part.ressourceNb;
            if(coldLevel < 0){
              coldLevel = 0;
            }
          }else if(neededRessource == "R"){
            ragLevel -= part.ressourceNb;
            if(ragLevel < 0){
              ragLevel = 0;
            }
          }
        }
        
        
        forceGoHome();
      }
      
    }
    
  }

///
/// Début du calcul GOAP
///  
  
  /*
  
 
  //====================\\
  ||=====JUICY GOAP=====||
  \\====================//
  
  */
  
///
/// Recherche d'objectif en fonction de l'état initial du citoyen 
///  
  void findGoal(){
    
    //LIST OF STARTING STATES
    ArrayList<String> strings = new ArrayList<String>();
    
    //LIST OF PATH NODES
    ArrayList<StepNode> nodes = new ArrayList<StepNode>();
    
    //LIST OF STARTING NODES
    ArrayList<StepNode> startingNodes = new ArrayList<StepNode>();
    
    if(hungryLevel > CITIZEN_SATISFACTION_LIMIT){
      strings.add("hungryTrue");
    }
    if(coldLevel > CITIZEN_SATISFACTION_LIMIT){
      strings.add("coldTrue");
    }
    if(ragLevel > CITIZEN_SATISFACTION_LIMIT){
      strings.add("ragTrue");
    }
    
    //FIND STARTING NODES
    for(StepNode part : stepNodes){
      
      if(strings.contains(part.getPreCondition())){
        
        startingNodes.add(part);
        
      }
      
    }
    
    
    
    //=====FIND SECOND NODES=====\\
    for(StepNode part : startingNodes){
      
      for(StepNode opart : stepNodes){
        
        if(!startingNodes.contains(opart)){
          
          if(part.getPostCondition() == opart.getPreCondition()){
            nodes.add(opart);
          }
          
        }
        
      }
      
    }
    
    //=====FIND ALL PATH NODES=====\\
    boolean foundNode = false;
    
    do{
      
      foundNode = false;
      
      //copy to prevent concurent modification error
      ArrayList<StepNode> nodesCopy = new ArrayList<StepNode>();
      for(StepNode part : nodes) nodesCopy.add(part);
      
      for(StepNode part : stepNodes){
        
        if(!nodes.contains(part)){
          
          for(StepNode opart : nodesCopy){
            
            if(part.getPreCondition() == opart.getPostCondition()){
              nodes.add(part);
              foundNode = true;
            }
            
          }
          
        }
        
      }
      
    }while(foundNode);
    
    
    //=====CREATE COMPLETE PATHS=====\\
    
    //LIST OF NODE LISTS (PATHS)
    ArrayList<ArrayList<StepNode>> paths = new ArrayList<ArrayList<StepNode>>();
    
    //LIST OF PATH COSTS
    ArrayList<Integer> pathCosts = new ArrayList<Integer>();
    
    //=====CREATE ALL POSSIBLE STARTING PATHS=====\\
    for(int i = 0; i < startingNodes.size(); i++){
      ArrayList<StepNode> nPath = new ArrayList<StepNode>();
      nPath.add(startingNodes.get(i));
      paths.add(nPath);
    }
    
    //=====FIND AND COMPILE PATHS=====\\
    for(int i = 0; i < paths.size(); i++){
      
      ArrayList<StepNode> npath = paths.get(i);
      
      do{
      
        foundNode = false;
        
        //copy to prevent concurent modification error
        ArrayList<StepNode> nodesCopy = new ArrayList<StepNode>();
        for(StepNode part : npath) nodesCopy.add(part);
        
        for(StepNode part : nodesCopy){
          
          for(StepNode opart : nodes){
            
            if(part.getPostCondition() == opart.getPreCondition() && !npath.contains(opart)){
              npath.add(opart);
              foundNode = true;
            }
            
          }
          
        }
        
      }while(foundNode);
      
    }
    
    
    //=====CALCULATE PATH COST=====\\
    for(int i = 0; i < paths.size(); i++){
      
      ArrayList<StepNode> npath = paths.get(i);
      String type = "";
      String actionName = npath.get(0).actionName;
      if(actionName.contains(new String("FOOD"))){
        type = "F";
      }else if(actionName.contains(new String("COAL"))){
        type = "C";
      }else if(actionName.contains(new String("CLOTHES"))){
        type = "R";
      }
      
      int tempCost = 0;
      
      for(StepNode part : npath){
        if(type == "F"){
          tempCost += part.getCost(hungryLevel, distToClosest("F"));
        }else if(type == "C"){
          tempCost += part.getCost(coldLevel, distToClosest("C"));
        }else if(type == "R"){
          tempCost += part.getCost(ragLevel, distToClosest("R"));
        }
      }
      
      pathCosts.add(new Integer(tempCost));
      
    }
    
    ArrayList<Boolean> validPath = new ArrayList<Boolean>();
    
    
    //=====CHECK POSSIBILITY OF PATHS=====\\
    for(ArrayList<StepNode> npaths : paths){
      
      boolean isValid = true;
      
      for(StepNode part : npaths){
        
        if(part.getPreCondition() == "foodAvailableTrue"){
          if(!hasRessourceInRange("F")){
            isValid = false;
          }
        }else if(part.getPreCondition() == "coalAvailableTrue"){
          if(!hasRessourceInRange("C")){
            isValid = false;
          }
        }else if(part.getPreCondition() == "clothesAvailableTrue"){
          if(!hasRessourceInRange("R")){
            isValid = false;
          }
        }
        
      }
      
      validPath.add(isValid);
      
    }
    
    int lowestIndex = -1;
    int lowestCost = 500000;
    
    for(int i = 0; i < paths.size(); i++){
      
      if(validPath.get(i) == Boolean.TRUE){
        
        if(pathCosts.get(i) < lowestCost){
          lowestCost = pathCosts.get(i);
          lowestIndex = i;
        }
        
      }
      
    }
    
    
    if(lowestIndex != -1){
      
      StepNode lastNode = paths.get(lowestIndex).get(paths.get(lowestIndex).size()-1);
      
      String actionName = lastNode.actionName;
      if(actionName == "WALK TO HOME WITH FOOD"){
        neededRessource = "F";
        findClosestRessource();
        makeHappyParticle();
      }else if(actionName == "RECEIVE FREE FOOD"){
        neededRessource = "F";
        forceGoHome();
        makeMadParticle();
      }else if(actionName == "WALK TO HOME WITH COAL"){
        neededRessource = "C";
        findClosestRessource();
        makeHappyParticle();
      }else if(actionName == "RECEIVE FREE COAL"){
        neededRessource = "C";
        forceGoHome();
        makeMadParticle();
      }else if(actionName == "WALK TO HOME WITH CLOTHES"){
        neededRessource = "R";
        findClosestRessource();
        makeHappyParticle();
      }else if(actionName == "RECEIVE FREE CLOTHES"){
        neededRessource = "R";
        forceGoHome();
        makeMadParticle();
      }
      
      politician.alterAnger(lastNode.getGain());
      
    }else {
      currentState.goHome();
      politician.alterAnger(-2);
    }
    
    
    
  }
  
  /** Avis à l'observateur de générer une particule content
  */
  void makeHappyParticle(){
    
    pnotifyObs();
    particleID = PARTICLE_HAPPY;
    spawnPos = new PVector(citizenPosition.x, citizenPosition.y - CITIZEN_RECTANGLE_SIZE);
    
  }
  
  /** Avis à l'observateur de générer une particule fâché
  */
  void makeMadParticle(){
    
    pnotifyObs();
    particleID = PARTICLE_MAD;
    spawnPos = new PVector(citizenPosition.x, citizenPosition.y - CITIZEN_RECTANGLE_SIZE);
    
  }
  
  /** Avis à l'observateur de générer une particule mort
  */
  void makeDeadParticle(){
    
    pnotifyObs();
    particleID = PARTICLE_DEAD;
    spawnPos = new PVector(citizenPosition.x, citizenPosition.y - CITIZEN_RECTANGLE_SIZE);
    
  }
  
  /** Avis à l'observateur de générer une particule confus
  */
  
  void makeConfusedParticle(){
    
    pnotifyObs();
    particleID = -1;
    spawnPos = new PVector(citizenPosition.x, citizenPosition.y - CITIZEN_RECTANGLE_SIZE);
    
  }
  
  /** Distribution normale pour les tolérances de besoin
  */
  
  void generateGaussianValues(){
    
    foodAddRate = 500;
    
    foodAddRate += randomGaussian() * 75;
    
    
    coalAddRate = 1000;
    
    coalAddRate += randomGaussian() * 200;
    
    
    ragAddRate = 2000;
    
    ragAddRate += randomGaussian() * 750;
    
    
    
    //goapRate += randomGaussian() * 2000;
    
  }
  
  
}

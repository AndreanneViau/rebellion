///
/// Déclaration des variables
///  


ArrayList<Citizen> citizens;
ArrayList<StepNode> stepNodes;

PVector mousePosition;

Ressources ressources;

RessourceFactory ressourceFactory;
CitizenFactory citizenFactory;

Politician politician;

Particles particles;


GameState gameState;

int foodAmount;

String spawnType;

void createAll(){
  
  createRessources();
  createCitizens();
  createNodes();
  
  politician = new Politician();
  politician.setBoldType("F");
  
  gameState = new GameState();
  gameState.addObservable(politician);
  
  particles = new Particles();
  for(Citizen part : citizens){
    particles.addObservable(part);
  }
  
  spawnType = "F";
  
}




void createRessources(){
  
  foodAmount++;
  ressources = new Ressources();
  ressourceFactory = new RessourceFactory();
  
}


void createCitizens(){
  
  citizens = new ArrayList<Citizen>();
  citizenFactory = new CitizenFactory();
  for (int i = 0; i<NUMBER_OF_CITIZENS; i++) {
    
    addCitizen();
    
  }
  
}

void addCitizen(){
  
  Citizen citizen = citizenFactory.addCitizen();
  if(particles != null){
    particles.addObservable(citizen);
  }
  if(ressources != null){
    ressources.addObservable(citizen);
  }
  citizens.add(citizen);
  
}


void createNodes(){
  
  stepNodes = new ArrayList<StepNode>();
  StepNode stepNode;

  // GOAP NODE CREATOR  
  //( String NactionName, String NpreCondition, String NpostCondition, int Ncost, int Ngain, int Nmultiplicator)





  stepNode = new StepNode(1, "SEARCH FOOD AVAILABILITY F", "hungryTrue", "foodAvailableTrue", 5, 0, false, true);
  stepNodes.add(stepNode);
  stepNode = new StepNode(2, "CALCULATE ROUTE TO FOOD TARGET", "foodAvailableTrue", "onTheWayFoodTrue", 5, 0, false, false);
  stepNodes.add(stepNode);
  stepNode = new StepNode(3, "WALK TO TARGET WITH EMPTY HANDS", "onTheWayFoodTrue", "reachFoodTrue", 1, 0, true, false);
  stepNodes.add(stepNode);
  stepNode = new StepNode(4, "TAKE FOOD", "reachFoodTrue", "foodTakenTrue", 15, 0, false, false);
  stepNodes.add(stepNode);
  stepNode = new StepNode(5, "CALCULATE ROUTE TO HOME", "foodTakenTrue", "onTheWayWithFoodTrue", 3, 0, false, false);
  stepNodes.add(stepNode);
  stepNode = new StepNode(6, "WALK TO HOME WITH FOOD", "onTheWayWithFoodTrue", "hungryFalse", 2, -2, true, false);
  stepNodes.add(stepNode);
  
  
  
  stepNode = new StepNode(7, "SEARCH FOOD AVAILABILITY T", "hungryTrue", "foodAvailableFalse", 5, 0, false, true);
  stepNodes.add(stepNode);
  stepNode = new StepNode(8, "MANIFESTATION FOR FOOD", "foodAvailableFalse", "hatePoliticiansFoodTrue", 5, 0, false, true);
  stepNodes.add(stepNode);
  stepNode = new StepNode(9, "RECEIVE FREE FOOD", "hatePoliticiansFoodTrue", "hungryFalse", 0, 5, false, false);
  stepNodes.add(stepNode);




  stepNode = new StepNode(10, "SEARCH COAL AVAILABILITY", "coldTrue", "coalAvailableTrue", 3, 0, false, false);
  stepNodes.add(stepNode);
  stepNode = new StepNode(11, "CALCULATE ROUTE TO COAL TARGET", "coalAvailableTrue", "onTheWayCoalTrue", 5, 0, false, false);
  stepNodes.add(stepNode);
  stepNode = new StepNode(26, "WALK TO TARGET WITH EMPTY HANDS", "onTheWayCoalTrue", "reachCoalTrue", 1, 0, true, false);
  stepNodes.add(stepNode);
  stepNode = new StepNode(12, "TAKE COAL", "reachCoalTrue", "coalTakenTrue", 10, 0, false, false);
  stepNodes.add(stepNode);
  stepNode = new StepNode(23, "CALCULATE ROUTE TO HOME", "coalTakenTrue", "onTheWayWithCoalTrue", 3, 0, false, false);
  stepNodes.add(stepNode);
  stepNode = new StepNode(13, "WALK TO HOME WITH COAL", "onTheWayWithCoalTrue", "coldFalse", 5, -5, true, false);
  stepNodes.add(stepNode);




  stepNode = new StepNode(18, "SEARCH COAL AVAILABILITY", "coldTrue", "coalAvailableFalse", 3, 0, false, true);
  stepNodes.add(stepNode);
  stepNode = new StepNode(14, "MANIFESTATION FOR COAL", "coalAvailableFalse", "hatePoliticiansCoalTrue", 5, 0, false, true);
  stepNodes.add(stepNode);
  stepNode = new StepNode(15, "RECEIVE FREE COAL", "hatePoliticiansCoalTrue", "coldFalse", 0, 3, false, false);
  stepNodes.add(stepNode);

  


  stepNode = new StepNode(16, "SEARCH CLOTHES AVAILABILITY", "ragTrue", "clothesAvailableTrue", 1, 0, false, false);
  stepNodes.add(stepNode);
  stepNode = new StepNode(17, "CALCULATE ROUTE TO CHOTHES TARGET", "clothesAvailableTrue", "onTheWayClothesTrue", 5, 0, false, false);
  stepNodes.add(stepNode);
  stepNode = new StepNode(27, "WALK TO TARGET WITH EMPTY HANDS", "onTheWayClothesTrue", "reachClothesTrue", 1, 0, true, false);
  stepNodes.add(stepNode);
  stepNode = new StepNode(19, "TAKE CLOTHES", "reachClothesTrue", "clothesTakenTrue", 5, 0, false, false);
  stepNodes.add(stepNode);
  stepNode = new StepNode(24, "CALCULATE ROUTE TO HOME", "clothesTakenTrue", "onTheWayWithClothesTrue", 3, 0, false, false);
  stepNodes.add(stepNode);
  stepNode = new StepNode(20, "WALK TO HOME WITH CLOTHES", "onTheWayWithClothesTrue", "ragFalse", 1, -10, true, false);
  stepNodes.add(stepNode);



  stepNode = new StepNode(25, "SEARCH CLOTHES AVAILABILITY", "ragTrue", "clothesAvailableFalse", 1, 0, false, true);
  stepNodes.add(stepNode);
  stepNode = new StepNode(21, "MANIFESTATION FOR CLOTHES", "clothesAvailableFalse", "hatePoliticiansClothesTrue", 5, 0, false, true);
  stepNodes.add(stepNode);
  stepNode = new StepNode(22, "RECEIVE FREE CLOTHES", "hatePoliticiansClothesTrue", "ragFalse", 0, 2, false, false);
  stepNodes.add(stepNode);

 // listStepNode();
  
}



void listStepNode() {

  for (StepNode s : stepNodes) { 
    s.display();
  }
}

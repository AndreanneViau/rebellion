/** Gestionnaire des particules
*/

class Particles extends Observer{
  
  ArrayList<EmotionParticle> particles;
  
  Particles(){
    
    obs = new ArrayList<Observable>();
    particles = new ArrayList<EmotionParticle>();
    
  }
  
  void update(float deltaTime){
    
    for(Observable part : obs){
      
      if(((ParticleObservable)part).pisNotified()){
        
        particles.add(new EmotionParticle(((ParticleObservable)part).spawnPos, ((ParticleObservable)part).particleID));
        ((ParticleObservable)part).pconfirmNotification();
        
      }
      
    }
    
    for(int i = particles.size() - 1; i >= 0; i--){
      
      EmotionParticle part = particles.get(i);
      
      part.update(deltaTime);
      
      if(!part.isAlive()){
        particles.remove(i);
      }
      
    }
    
  }
  
  
  void show(){
    
    for(EmotionParticle part : particles){
      part.show();
    }
    
  }
  
}

class StateHandler{
  
  State state;
  
  StateHandler(){
    state = new StateAtHome();
  }
  
  void handle(){}
  void setState(State nstate){state = nstate;}
  
  
  void goHome(){
    state = new StateToHome();
  }
  void atHome(){
    state = new StateAtHome();
  }
  void fetching(){
    state = new StateFetching();
  }
  
  String getCurrentState(){return state.getType();}
  
}

abstract class State{
  
  abstract void handle(StateHandler handler);
  abstract String getType();
  
}


class StateAtHome extends State{
  
  void handle(StateHandler handler){
    goGetRessource(handler);
  }
  
  void goGetRessource(StateHandler handler){
    handler.setState(new StateFetching());
  }
  
  String getType(){
    return this.getClass().getSimpleName();
  }
  
}

class StateFetching extends State{
  
  void handle(StateHandler handler){
    goHome(handler);
  }
  
  void goHome(StateHandler handler){
    handler.setState(new StateToHome());
  }
  
  String getType(){
    return this.getClass().getSimpleName();
  }
  
}

class StateToHome extends State{
  
  void handle(StateHandler handler){
    atHome(handler);
  }
  
  void atHome(StateHandler handler){
    handler.setState(new StateAtHome());
  }
  
  String getType(){
    return this.getClass().getSimpleName();
  }
  
}

/** Patron d'une entité qui observe les observables
*/

abstract class Observer{
  
  protected ArrayList<Observable> obs;
  
  /** Ajout d'une entité observable aux entités observées
  */
    public void addObservable(Observable nObs){
    if(nObs != null && obs != null){
      obs.add(nObs);
    }
  }
  
}

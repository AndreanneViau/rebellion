/** Observable qui notifie que la ressource a été prise
*/

abstract class FoodObservable extends Observable{
  
  protected int foodID;
  
}

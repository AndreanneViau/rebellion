final float FRAME_RATE = 60;
final float FRAME_DELTA = 1000/FRAME_RATE;
float lastDisplay =0;

final boolean DEBUG = true;

//=====CITIZEN=====\\

final int NUMBER_OF_CITIZENS = 10;
final float CITIZEN_SPEED = 1;
final float CITIZEN_RESSOURCE_RANGE_RATIO = 5;

final int CITIZEN_RECTANGLE_SIZE = 8;
final int CITIZEN_HOUSE_SIZE = 30;

final int CITIZEN_SATISFACTION_LIMIT = 20;


//=====RESSOURCES=====\\

final int RESSOURCE_SIZE = 6;

final float RESSOURCE_FOOD_PRICE = 0.75;
final float RESSOURCE_COAL_PRICE = 1.25;
final float RESSOURCE_CLOT_PRICE = 1.75;



//=====BUTTONS=====\\


final int BUTTONS_WIDTH_RATIO = 12;
final int BUTTONS_HEIGHT_RATIO = 16;



//=====VIGNETTE=====\\


final int VIGNETTE_DISTANCE_RATIO = 10;


//=====PARTICLES=====\\

final float PARTICLE_SIZE_RATIO = 100;
final int PARTICLE_HAPPY = 0;
final int PARTICLE_MAD = 1;
final int PARTICLE_DEAD = 2;

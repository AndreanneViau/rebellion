///
/// Utilisation du DP de la fabrique pour générer les citoyens afin de permettre de la flexibilité lors de la création au moment de modifications
///  


class CitizenFactory{
  
  float lastAdded;
  float addRate;
  
  boolean canAdd;
  
  CitizenFactory(){
    
    lastAdded = 0;
    addRate = 10000;
    
    canAdd = false;
    
  }
  
  boolean canAddCitizen(){return canAdd;}
  
///
/// mise à jour de la fabrique afin de déterminer la cadence de création de nouveaux citoyens
///  
  void update(){
    
    if(currentTime - lastAdded >= addRate){
      
      canAdd = true;
    }
    
  }
  
  
  
///
/// Ajout d'un citoyen aux ressources du jeu
///  
  
  Citizen addCitizen(){
    
    lastAdded = currentTime;
    canAdd = false;
    
    Citizen citizen = new Citizen();
    citizen.citizenPosition.x = random(15, width-15);
    citizen.citizenPosition.y = random(15, height-height/BUTTONS_HEIGHT_RATIO);
    citizen.housePosition.x = citizen.citizenPosition.x;
    citizen.housePosition.y = citizen.citizenPosition.y;
    citizen.forceGoHome();
    
    return citizen;
    
  }
  
  
}

/** Classe pour générer les particules représentant les émotions des citoyens.  Les  particules sont créées à partir du DP Template
*/  


class EmotionParticle extends Particle{
  
///
/// Constructeur des particules d'émotion, on appelle le constructeur du template et on ajoute les attributs nécessaires à l'utilisation définie
///   
  EmotionParticle(PVector npos, int ntype){
    
    super();
    
    pos = npos.copy();
    vel = new PVector(0, -0.5);
    
    size = new PVector(width/PARTICLE_SIZE_RATIO, width/PARTICLE_SIZE_RATIO);
    
    type = ntype;
    
  }
  
/**
* Mise à jour des particules 
*/ 
  void update(float deltaTime){
    
    pos.add(vel);
    
    if(life > 0){
      life -= 2;
    }else {
      alive = false;
    }
    
    
  }
  
  
/** Affichage des particules en fonction de l'émotion
*/   
  void show(){
    
    pushMatrix();
    translate(0, 0, 4);
    
    colorMode(RGB, 255, 255, 255, 255);
    stroke(0, life);
    
    switch(type){
      
      case(PARTICLE_HAPPY):
        
        
        textAlign(CENTER, CENTER);
        fill(0, life);
        textSize(17);
        text("●", pos.x-2, pos.y);
        fill(255, 150, 150, life);
        translate(0, 0, 1);
        textSize(15);
        text("☻", pos.x, pos.y);
        textAlign(LEFT, BASELINE);
        
        break;
      case(PARTICLE_MAD):
        
        
        textAlign(CENTER, CENTER);
        fill(0, life);
        textSize(17);
        text("▼", pos.x, pos.y);
        fill(255, 255, 75, life);
        translate(0, 0, 1);
        textSize(9);
        text("▼", pos.x, pos.y);
        textAlign(LEFT, BASELINE);
        
        break;
      case(PARTICLE_DEAD):
        
        fill(0, 0, 0, life);
        
        textAlign(CENTER, CENTER);
        textSize(17);
        text("x_x", pos.x, pos.y);
        textAlign(LEFT, BASELINE);
        
        break;
      default:
        
        fill(0, 0, 0, life);
        textSize(17);
        text("?", pos.x, pos.y);
        
        break;
      
    }
    
    
    popMatrix();
    
  }
  
}
